package root.services;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.persitence.entites.TieJuegos;

@Path("/juegos")
public class JuegosREST {
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("UP_juegos");
    EntityManager em;
     
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarTodo(){
        em = emf.createEntityManager();
        
        List <TieJuegos> lista =  em.createNamedQuery("TieJuegos.findAll").getResultList();
        
        return Response.ok().entity(lista).build();
    }
    
    @GET
    @Path("/{idbuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarId(@PathParam("idbuscar") int idbuscar){
        em = emf.createEntityManager();
        TieJuegos juego = em.find(TieJuegos.class, idbuscar);
        em.close();
        return Response.ok(200).entity(juego).build();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public String agregarJuego(TieJuegos juegoNuevo){
        em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(juegoNuevo);
        em.getTransaction().commit();
        em.close();
        return "Juego Guardado";
    }
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response actulizarJuego(TieJuegos juegoUpdate){
        
        em = emf.createEntityManager();
        em.getTransaction().begin();
        juegoUpdate = em.merge(juegoUpdate);
        em.getTransaction().commit();
        return Response.ok(juegoUpdate).build();
    }
    
    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eliminarPorId(@PathParam("id") int id){
        em = emf.createEntityManager();
        em.getTransaction().begin();
        TieJuegos juegoAEliminar = em.getReference(TieJuegos.class, id);
        em.remove(juegoAEliminar);
        em.getTransaction().commit();
        return Response.ok("Juego Eliminado").build();
    }
}
