/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.persitence.entites;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "tie_juegos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TieJuegos.findAll", query = "SELECT t FROM TieJuegos t"),
    @NamedQuery(name = "TieJuegos.findById", query = "SELECT t FROM TieJuegos t WHERE t.id = :id"),
    @NamedQuery(name = "TieJuegos.findByNombre", query = "SELECT t FROM TieJuegos t WHERE t.nombre = :nombre"),
    @NamedQuery(name = "TieJuegos.findByDescripcion", query = "SELECT t FROM TieJuegos t WHERE t.descripcion = :descripcion"),
    @NamedQuery(name = "TieJuegos.findByPrecio", query = "SELECT t FROM TieJuegos t WHERE t.precio = :precio"),
    @NamedQuery(name = "TieJuegos.findByPlataforma", query = "SELECT t FROM TieJuegos t WHERE t.plataforma = :plataforma"),
    @NamedQuery(name = "TieJuegos.findByCategoria", query = "SELECT t FROM TieJuegos t WHERE t.categoria = :categoria")})
public class TieJuegos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "precio")
    private Integer precio;
    @Column(name = "plataforma")
    private String plataforma;
    @Column(name = "categoria")
    private String categoria;

    public TieJuegos() {
    }

    public TieJuegos(Integer id) {
        this.id = id;
    }

    public TieJuegos(Integer id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getPrecio() {
        return precio;
    }

    public void setPrecio(Integer precio) {
        this.precio = precio;
    }

    public String getPlataforma() {
        return plataforma;
    }

    public void setPlataforma(String plataforma) {
        this.plataforma = plataforma;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TieJuegos)) {
            return false;
        }
        TieJuegos other = (TieJuegos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.persitence.entites.TieJuegos[ id=" + id + " ]";
    }
    
}
